# control-computer-app-client
> see JavaDoc: [JavaDoc](docs/javadoc/index.html)

## Description
A client used to control a computer server

### Tags
[Java, Application]
